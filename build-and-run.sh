#!/bin/sh

( cd sw && make ) || exit 1
( cd host && make ) || exit 1
( cd hw && make ) || exit 1

./sdram-load-and-run.sh || exit 1

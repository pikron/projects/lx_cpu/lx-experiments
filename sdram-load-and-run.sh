#!/bin/bash

set -x

if [ -z "$USB_SENDHEX" ] ; then
  USB_SENDHEX=host/_compiled/bin/usb_sendhex
fi

# Reset to BL
$USB_SENDHEX -d 0x1669:0x1027 -r # Fails if we're already in bootloader
#sleep 2

# Send binary
$USB_SENDHEX -w -d 0xdead:0x2263 -t 1 -s 0xA0000000 -f binary sw/_compiled/bin/lx_exp_base-sdram.bin
#sleep 2

# Run binary
$USB_SENDHEX -d 0xdead:0x2263 -g `$USB_SENDHEX -d 0xdead:0x2263 -s 0xA0000004 -l 4 -t 1 -u -f dump - \
  | sed -n -e 's/^.*:\(..\) \(..\) \(..\) \(..\) */0x\4\3\2\1/p'`
  # FIXME: broken pipe because targed disconnect when jumps to new code
sleep 3

# Upload FPGA
$USB_SENDHEX -w -d 0x1669:0x1027 -t 1 -s 0xA1C00000 -f binary hw/_build/lx_exp_base.pkg
#sleep 2

# Configure FPGA
$USB_SENDHEX -d 0x1669:0x1027 -c 0xF000
sleep 2

# Upload firmware
#$USB_SENDHEX -w -d 0x1669:0x1027 -t 1 -s 0xA1C00000 -f binary fw_lxmaster.bin
#$USB_SENDHEX -d 0x1669:0x1027 -c 0xF024 -a `stat -L -c %s fw_lxmaster.bin`

# Enable IRC
#$USB_SENDHEX -d 0x1669:0x1027 -c 0xF011 -a 0

# Configure LX Master
#$USB_SENDHEX -d 0x1669:0x1027 -c 0xF023 -a 0

# Enable LX Master
#$USB_SENDHEX -d 0x1669:0x1027 -c 0xF021 -a 0

if false ; then
# Test FPGA memory access
$USB_SENDHEX -w -d 0x1669:0x1027 -t 4 -s 0x80000000 -f binary rand2k.bin

$USB_SENDHEX -w -d 0x1669:0x1027 -t 4 -s 0x80000000 -l 0x800 -u -f binary rback.bin
if ! cmp -s rand2k.bin rback.bin ; then
  echo "Pattern readback error"
  exit 1
fi
fi
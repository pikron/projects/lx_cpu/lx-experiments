/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  cmd_uartcon.c - interconnection of text command processor
                with RS-232 line

  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <utils.h>
#include <cmd_proc.h>

#define ED_LINE_CHARS 512

cmd_io_t cmd_io_uartcon_dev;

char ed_line_chars_uartcon_in[ED_LINE_CHARS + 1];
char ed_line_chars_uartcon_out[ED_LINE_CHARS + 1];

ed_line_buf_t ed_line_buf_uartcon_in =
{
flg:
  FL_ELB_ECHO,
  inbuf: 0,
alloc:
  sizeof(ed_line_chars_uartcon_in),
  maxlen: 0,
  lastch: 0,
buf:
  ed_line_chars_uartcon_in
};

ed_line_buf_t ed_line_buf_uartcon_out =
{
flg:
  FL_ELB_NOCRLF,
  inbuf: 0,
alloc:
  sizeof(ed_line_chars_uartcon_out),
  maxlen: 0,
  lastch: 0,
buf:
  ed_line_chars_uartcon_out
};

cmd_io_t cmd_io_uartcon =
{
putc:
  cmd_io_line_putc,
getc:
  NULL,
write:
  cmd_io_write_bychar,
read:
  NULL,
priv:
  {
  ed_line:
    {
    in:
      &ed_line_buf_uartcon_in,
    out:
      &ed_line_buf_uartcon_out,
    io_stack:
      &cmd_io_uartcon_dev
    }
  }
};

int cmd_io_putc_uartcon(struct cmd_io *cmd_io, int ch)
{
  int uartch = cmd_io->priv.uart.uartch;

  ch = uart0PutchNW(ch);

  if (ch == -1)
    return -1;
  else
    return ch;
}

int cmd_io_getc_uartcon(struct cmd_io *cmd_io)
{
  int ch;
  int uartch = cmd_io->priv.uart.uartch;

  ch = uart0Getch();

  if (ch == -1)
    return -1;
  else
    return ch;
}

cmd_io_t cmd_io_uartcon_dev =
{
  .putc = cmd_io_putc_uartcon,
  .getc = cmd_io_getc_uartcon,
  .write = cmd_io_write_bychar,
  .read = cmd_io_read_bychar,
  .priv.uart = { -1}
};


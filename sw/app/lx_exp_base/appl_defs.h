#ifndef _TEST_LPC_H
#define _TEST_LPC_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include "appl_config.h"

#include <stdint.h>
#include <lt_timer.h>

#ifdef CONFIG_APP_LX_EXP_BASE_WITH_SUITK
#define APPL_WITH_SUITK
#endif /*CONFIG_APP_LX_EXP_BASE_WITH_SUITK*/

#ifdef CONFIG_APP_LX_EXP_BASE_WITH_SIM_POSIX
#define APPL_WITH_SIM_POSIX
#define APPL_WITH_FINALIZATION_CHECK
#endif /*CONFIG_APP_LX_EXP_BASE_WITH_SIM_POSIX*/

#ifdef CONFIG_APP_LX_EXP_BASE_WITH_FPGA
#define APPL_WITH_FPGA
#endif /*CONFIG_APP_LX_EXP_BASE_WITH_FPGA*/

#ifdef CONFIG_APP_LX_EXP_BASE_WITH_USB
#define APPL_WITH_USB
#endif /*CONFIG_APP_LX_EXP_BASE_WITH_USB*/

#ifdef CONFIG_APP_LX_EXP_BASE_WITH_LWIP
#define APPL_WITH_LWIP
int lwip_app_init(void);
int lwip_app_poll(void);
#endif /*CONFIG_APP_ROCON_WITH_LWIP*/

#ifdef CONFIG_APP_LX_EXP_BASE_WITH_UART1CMD
#define APPL_WITH_UART1CMD
#endif

#ifdef CONFIG_APP_LX_EXP_BASE_WITH_ULAN
#define APPL_WITH_ULAN
#include <ul_lib/ulan.h>
#include <uloi_base.h>
#ifdef CONFIG_ULAN_DY
  #include <uldy_base.h>
  extern ul_dyac_t *ul_dyac;
#endif /*CONFIG_ULAN_DY*/
/* ulan identification */
extern const char *ul_idstr;
/* ulan variables */
extern ul_fd_t ul_fd,ul_fd1;
extern ul_msginfo umsginfo;
extern uloi_coninfo_t *coninfo;
#endif /*CONFIG_APP_LX_EXP_BASE_WITH_ULAN*/

extern int usb_enable_flag;

extern int app_exit_request;

#ifdef CONFIG_OC_I2C_DRV_SYSLESS
#define APPL_EEPROM_ADDR_SIZE 2

#define DISTORE_EEPROM_I2C_ADDR 0xA0

#define DISTORE_EEPROM_SIGNATURE 0x51
#define DISTORE_EEPROM_HEADER_SIZE 6

#define DISTORE_EEPROM_PAGE 0x0020
#define DISTORE_EEPROM_SIZE 0x2000
#define DISTORE_EEPROM_USER_START 0x100
#define DISTORE_EEPROM_USER_SIZE  0x400
/* user end 0x100 + 2 * 0x400 = 0x900 */

/*
#define APPL_WITH_DISTORE_EEPROM_USER
#define appl_distore_eeprom_user_des lcd_distore_user_des
int appl_distore_init(void);
int appl_distore_user_set_check4change(void);
int appl_distore_user_restore(void);
int appl_distore_user_change_check(void);
*/

/*
#define APPL_WITH_TIMEPROG_EEPROM_STORE
#define TIMEPROG_EEPROM_STORE_START 0x0900
#define TIMEPROG_EEPROM_STORE_SIZE  0x1700
*/

#define appl_timeprog_eeprom_identifier lcd_timeprog
long timeprog_stored_size;
int appl_timeprog_eeprom_restore(void);
#endif /*CONFIG_OC_I2C_DRV_SYSLESS*/

#define APPL_TIMEPROG_AREA_SIZE 0x1700

#ifdef CONFIG_KEYVAL
/*
#define KVPB_KEYID_SETUP_SERVICE	0x104
#define KVPB_SETUP_SERVICE_MAX_SIZE	1024
#define APPL_WITH_DISTORE_KEYVAL_SERVICE
#define setup_distore_service_des lcd_distore_service_des
int setup_distore_restore(void);
int setup_distore_store(void);
*/
#endif /*CONFIG_KEYVAL*/

struct mtd_spi_state_t;

extern struct mtd_spi_state_t mtd_spi_state;

LT_TIMER_DEC(lt_10msec)

#ifdef APPL_WITH_ULAN
extern const uloi_objdes_array_t uloi_objdes_main;
#endif /*APPL_WITH_ULAN*/

void mloop(void);

int gui_init(void);

int gui_poll(void);

#define APPL_WITH_GUI_POLL_EXTENSION

int cmdproc_init(void);

int cmdproc_poll(void);

void save_cfg(void);

extern int usb_loadder_active;

int usb_app_init(void);

int usb_app_poll(void);

int usb_app_fill_serial_number(uint32_t ul_sn);

#define APPL_ACT_STOP          1
#define APPL_ACT_STOP_CER      2
#define APPL_ACT_MOTSTART      3
#define APPL_ACT_MOTSTART_CER  4
#define APPL_ACT_PURGE         5
#define APPL_ACT_PRGRUN        6
#define APPL_ACT_PRGEND        7
#define APPL_ACT_ERRCLR        8
#define APPL_ACT_PRGEND_PREP   9
#define APPL_ACT_PRGHOLD      10
#define APPL_ACT_PRGCONT      11
#define APPL_ACT_SETUP_SAVE   12
#define APPL_ACT_SERVICE_SAVE 13

int appl_global_action(int action_code);

/* LPC Quadrature encoder events processing */
struct lpc_qei_state_t;
void pxmc_lpc_qei_callback_index(struct lpc_qei_state_t *qst, void *context);
void lpc_qei_callback_cmpos0(struct lpc_qei_state_t *qst, void *context);
void lpc_qei_callback_cmpos1(struct lpc_qei_state_t *qst, void *context);

void lcd_sfi_isr(void);
void lcd_sfi_slow_isr(void);

#define APPL_RUN_AT_FAST_SFI do { \
   /*lcd_sfi_isr();*/ \
   ; \
 } while(0)

#define APPL_RUN_AT_SLOW_SFI do { \
   /*lcd_sfi_slow_isr();*/ \
   ; \
 } while(0)

#define APPL_SLOW_SFI_USEC 10000

/* Interrupt of the unused peripheral which is used to activate slow IRQ handling */
#define SLOW_SFI_INVOKE_IRQn SSP2_IRQn

#ifndef CONFIG_APP_LX_EXP_BASE_WITH_SIM_POSIX
void kbd_lc61spi_SetIndicatorsState(unsigned long mask, unsigned long xor_state);
#define kbd_SetIndicatorsState kbd_lc61spi_SetIndicatorsState
#endif
static inline void appl_update_indicators(void)
{
  /*lcd_update_indicators();*/
}
extern int lcd_backlight_level;
int lcd_set_constrast_level(int level);

#define KBD_LED_RUN_b    15
#define KBD_LED_HOLD_b   14
#define KBD_LED_PURGE_b  13
#define KBD_LED_ERROR_b  12
#define KBD_LED_A_b       6
#define KBD_LED_B_b       5
#define KBD_LED_C_b       4
#define KBD_LED_D_b       3
#define KBD_LED_MOTOR_b   1

#define KBD_LED_BACKLO_b 11
#define KBD_LED_BACKHI_b 10
#define KBD_LED_BEEPF_b   9
#define KBD_LED_BEEPN_b   8

#define KBD_LED_RUN_m	(1 << KBD_LED_RUN_b)
#define KBD_LED_HOLD_m	(1 << KBD_LED_HOLD_b)
#define KBD_LED_PURGE_m	(1 << KBD_LED_PURGE_b)
#define KBD_LED_ERROR_m	(1 << KBD_LED_ERROR_b)
#define KBD_LED_A_m	(1 << KBD_LED_A_b)
#define KBD_LED_B_m	(1 << KBD_LED_B_b)
#define KBD_LED_C_m	(1 << KBD_LED_C_b)
#define KBD_LED_D_m	(1 << KBD_LED_D_b)
#define KBD_LED_MOTOR_m	(1 << KBD_LED_MOTOR_b)

#define KBD_LED_BACKLO_m (1 << KBD_LED_BACKLO_b)
#define KBD_LED_BACKHI_m (1 << KBD_LED_BACKHI_b)
#define KBD_LED_BEEPN_m	(1 << KBD_LED_BEEPN_b)
#define KBD_LED_BEEPF_m	(1 << KBD_LED_BEEPF_b)

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _TEST_LPC_H */


#ifndef _APPL_FPGA_H
#define _APPL_FPGA_H

#include "appl_defs.h"

/* Register defines */

/* Configuration defines */

#define FPGA_CONFIGURATION_FILE_ADDRESS 0xA1C00000

#define FPGA_CONF_SUCESS              0
#define FPGA_CONF_ERR_RECONF_LOCKED   1
#define FPGA_CONF_ERR_RESET_FAIL      2
#define FPGA_CONF_ERR_WRITE_ERR       3
#define FPGA_CONF_ERR_CRC_ERR         4

int (*fpga_reconfiguaration_initiated)(void);
int (*fpga_reconfiguaration_finished)(void);

void fpga_init();
int fpga_configure();
int fpga_measure_bus_read();
int fpga_measure_bus_write();
void fpga_set_reconfiguration_lock(int lock);
int fpga_get_reconfiguration_lock();

#endif /*_APPL_FPGA_H*/

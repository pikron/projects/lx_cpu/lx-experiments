#include "appl_defs.h"
#include <string.h>
#include <ul_log.h>

UL_LOG_CUST(ulogd_dad)

void appl_global_action_store_user_heper()
{
 #ifdef APPL_WITH_DISTORE_EEPROM_USER
  appl_distore_user_set_check4change();
  appl_distore_user_change_check();
 #endif /*APPL_WITH_DISTORE_EEPROM_USER*/
}

int appl_global_action(int action_code)
{
  int res;

  switch(action_code) {

    case APPL_ACT_ERRCLR:
      return 0;

    case APPL_ACT_SETUP_SAVE:
      appl_global_action_store_user_heper();
      break;

    case APPL_ACT_SERVICE_SAVE:
     #ifdef APPL_WITH_DISTORE_KEYVAL_SERVICE
      setup_distore_store();
     #endif /*APPL_WITH_DISTORE_KEYVAL_SERVICE*/
      break;

  }
  return 0;
}

/*
 * This is generated file, do not edit it directly.
 * Take it from standard output of "ul_log_domains"
 * script called in the top level project directory
 */
ul_log_domain_t ulogd_dad	= {UL_LOGL_DEF, "dad"};

ul_log_domain_t *const ul_log_domains_array[] = {
  &ulogd_dad,
};

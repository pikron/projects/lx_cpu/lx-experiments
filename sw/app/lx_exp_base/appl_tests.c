#include <system_def.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <utils.h>
#include <cmd_proc.h>
#include <hal_gpio.h>
#include <hal_mpu.h>
#include <hal_machperiph.h>
#include <LPC17xx.h>
#include <spi_drv.h>
#include <lt_timer.h>

#include <ul_log.h>
#include <ul_logreg.h>

#include "appl_defs.h"

int cmd_do_test_memusage(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  void *maxaddr;
  char str[40];

  maxaddr = sbrk(0);

  snprintf(str,sizeof(str),"memusage maxaddr 0x%08lx\n",(unsigned long)maxaddr);
  cmd_io_write(cmd_io,str,strlen(str));

  return 0;
}

int cmd_do_test_adc(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  FILE *F;

  if (cmd_io->priv.ed_line.io_stack)
    cmd_io = cmd_io->priv.ed_line.io_stack;

  F = cmd_io_as_file(cmd_io, "r+");
  if (F == NULL)
    return CMDERR_EIO;

  fprintf(F, "ADC: %ld %ld %ld %ld %ld\n", (LPC_ADC->DR[0] & 0xFFF0) >> 4,
         (LPC_ADC->DR[1] & 0xFFF0) >> 4,
         (LPC_ADC->DR[2] & 0xFFF0) >> 4,
         (LPC_ADC->DR[3] & 0xFFF0) >> 4,
         (LPC_ADC->DR[7] & 0xFFF0) >> 4);
  fclose(F);
  return 0;
}

#ifdef APPL_WITH_DISTORE_EEPROM_USER
int cmd_do_test_distore(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  appl_distore_user_set_check4change();
  return 0;
}

int cmd_do_test_diload(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  appl_distore_user_restore();
  return 0;
}
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

int cmd_do_test_loglevel_cb(ul_log_domain_t *domain, void *context)
{
  char s[30];
  cmd_io_t *cmd_io = (cmd_io_t *)context;

  s[sizeof(s)-1]=0;
  snprintf(s,sizeof(s)-1,"%s (%d)\n\r",domain->name, domain->level);
  cmd_io_puts(cmd_io, s);
  return 0;
}

int cmd_do_test_loglevel(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res=0;
  char *line;
  line = param[1];

  if(!line||(si_skspace(&line),!*line)) {
    ul_logreg_for_each_domain(cmd_do_test_loglevel_cb, cmd_io);
  } else {
    res=ul_log_domain_arg2levels(line);
  }

  return res>=0?0:CMDERR_BADPAR;
}

int cmd_do_spimst_blocking(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res;
  int opchar;
  char *p=param[3];
  int spi_chan = (int)(intptr_t)des->info[0];
  uint8_t *tx_buff = NULL;
  uint8_t *rx_buff = NULL;
  long addr = 0;
  int len = 0;
  spi_drv_t *spi_drv;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar!=':')
    return -CMDERR_OPCHAR;

  if(spi_chan==-1)
    spi_chan=*param[1]-'0';

  spi_drv = spi_find_drv(NULL, spi_chan);
  if(spi_drv==NULL)
    return -CMDERR_BADSUF;

  p=param[3];

  si_skspace(&p);
  if(isdigit((int)*p)){
    if(si_long(&p,&addr,16)<0) return -CMDERR_BADPAR;
  }
  if(si_fndsep(&p,"({")<0) return -CMDERR_BADSEP;

  if((res=si_add_to_arr(&p, (void**)&tx_buff, &len, 16, 1, "})"))<0)
    return -CMDERR_BADPAR;

  rx_buff=malloc(len);

  res=-1;
  if(rx_buff!=NULL)
    res = spi_transfer_with_mode(spi_drv, addr, len, tx_buff, rx_buff, SPI_MODE_3);

  if(res < 0) {
    printf("SPI! %02lX ERROR\n",addr);
  } else {
    int i;
    printf("SPI! %02lX ",addr);
    printf("TX(");
    for(i=0;i<len;i++) printf("%s%02X",i?",":"",tx_buff[i]);
    printf(") RX(");
    for(i=0;i<len;i++) printf("%s%02X",i?",":"",rx_buff[i]);
    printf(")");
    printf("\n");
    return 0;
  }

  if(rx_buff)
    free(rx_buff);
  if(tx_buff)
    free(tx_buff);

  return 0;
}

int sdram_access_test(void)
{
  unsigned int *ptr;
  unsigned int pattern;
  size_t ramsz = SDRAM_SIZE;
  size_t cnt;
  lt_mstime_t tic;
  size_t blksz, i;

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;
  for (cnt = ramsz/sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;) {
    *(ptr++) = pattern;
    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM write %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;
  for (cnt = ramsz/sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;) {
    if(*ptr != pattern) {
      printf("SDRAM error modify at %p (%08x)\n", ptr, *ptr ^ pattern);
      return -1;
    }
    *(ptr++) = ~pattern;
    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM modify %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;
  for (cnt = ramsz/sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;) {
    if(*(ptr++) != ~pattern) {
      printf("SDRAM error read at %p (%08x)\n", ptr, *ptr ^ pattern);
      return -1;
    }
    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM read %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0;
  for (cnt = ramsz/sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;) {
    pattern += *(ptr++);
  }

  lt_mstime_update();
  printf("SDRAM sum %d ms res 0x%08x\n", (int)(lt_msdiff_t)(actual_msec - tic), pattern);

  for (blksz=1; blksz < 256 ; blksz *= 2) {
    lt_mstime_update();
    tic = actual_msec;

    pattern = 0;
    for (cnt = ramsz/sizeof(*ptr); cnt; cnt -= blksz) {
      ptr = (typeof(ptr))SDRAM_BASE;
      //ptr = (typeof(ptr))cmd_do_test_memusage;
      //ptr = (typeof(ptr))&ptr;
      for (i = blksz; i--; )
        pattern += *(ptr++);
    }
    lt_mstime_update();
    printf("SDRAM sum %d blksz %d ms res 0x%08x\n", (int)(lt_msdiff_t)(actual_msec - tic), (int)blksz, pattern);
  }

  return 0;
}

int cmd_do_testsdram(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  sdram_access_test();
  return 0;
}

int cmd_do_testmpu(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  int reg_cnt = hal_mpu_region_count();
  uint32_t ra, rsz;

  for (i = 0; i < reg_cnt; i++) {
    MPU->RNR = i;
    ra = MPU->RBAR & MPU_RBAR_ADDR_Msk;
    rsz = (1 << (__mfld2val(MPU_RASR_SIZE_Msk, MPU->RASR) + 1)) - 1;
    printf("R%d %08lX..%08lX %08lX %08lX\n", i, ra, ra + rsz, MPU->RBAR, MPU->RASR);
  }

  /*printf("IAP version %08X\n", lpcisp_read_partid());*/

  return 0;
}

int cmd_do_goaddr(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long addr;

  p = param[1];

  si_skspace(&p);
  if(si_ulong(&p,&addr,16)<0) return -CMDERR_BADPAR;

  printf("Jump to address %08lX\n\n", addr);

  ((void(*)(void))addr)();

  return 0;
}

cmd_des_t const cmd_des_test_memusage={0, 0,
                        "memusage","report memory usage",cmd_do_test_memusage,
                        {0,
                         0}};

cmd_des_t const cmd_des_test_adc={0, 0,
                        "testadc","adc test",cmd_do_test_adc,
                        {0,
                         0}};

#ifdef APPL_WITH_DISTORE_EEPROM_USER
cmd_des_t const cmd_des_test_distore={0, 0,
                        "testdistore","test DINFO store",cmd_do_test_distore,
                        {0,
                         0}};

cmd_des_t const cmd_des_test_diload={0, 0,
                        "testdiload","test DINFO load",cmd_do_test_diload,
                        {0,
                         0}};
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

cmd_des_t const cmd_des_test_loglevel={0, 0,
                        "loglevel","select logging level",
                        cmd_do_test_loglevel,{}};

cmd_des_t const cmd_des_spimst={0, CDESM_OPCHR|CDESM_WR,
			"SPIMST","SPI master communication request",
			cmd_do_spimst_blocking,{(void*)0}};

cmd_des_t const cmd_des_spimstx={0, CDESM_OPCHR|CDESM_WR,
			"SPIMST#","SPI# master communication request",
			cmd_do_spimst_blocking,{(void*)-1}};

cmd_des_t const cmd_des_testsdram={0, 0,
			"testsdram","test SDRAM",
			cmd_do_testsdram,{(void*)0}};

cmd_des_t const cmd_des_testmpu={0, 0,
			"testmpu","test MPU",
			cmd_do_testmpu,{(void*)0}};

cmd_des_t const cmd_des_goaddr={0, 0,
			"goaddr","run from address",
			cmd_do_goaddr,{(void*)0}};

cmd_des_t const *const cmd_appl_tests[]={
  &cmd_des_test_memusage,
  &cmd_des_test_adc,
 #ifdef APPL_WITH_DISTORE_EEPROM_USER
  &cmd_des_test_distore,
  &cmd_des_test_diload,
 #endif /*APPL_WITH_DISTORE_EEPROM_USER*/
  &cmd_des_test_loglevel,
  &cmd_des_spimst,
  &cmd_des_spimstx,
  &cmd_des_testsdram,
  &cmd_des_testmpu,
  &cmd_des_goaddr,
  NULL
};

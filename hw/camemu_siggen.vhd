library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lx_common_pkg.all;

-- Memory bus measurement
-- Holds the signal for one clock to simulate longest route

entity camemu_siggen is
	generic (
		h_size_max     : natural := 2500;
		v_size_max     : natural := 2500;
		clk_div        : natural := 2;
		pixel_bits     : natural := 10;
		pclk_act_edge  : std_logic := '1';
		vsync_act_lev  : std_logic := '1';
		hsync_act_lev  : std_logic := '1'
	);
	port
	(
		-- Clock
		clk_i          : in std_logic;
		-- Reset
		reset_i        : in std_logic;
		-- Parallel camera emulator interface
		camemu_data_o  : out std_logic_vector(pixel_bits - 1 downto 0);
		camemu_vsync_o : out std_logic;
		camemu_hsync_o : out std_logic;
		camemu_pclk_o  : out std_logic;
		camemu_wen_o   : out std_logic;
		camemu_field_o : out std_logic
	);
end camemu_siggen;

architecture Behavioral of camemu_siggen is

	-- HDW - Horizontal sync width
	constant par_hdw   : natural  := 10;
	-- PPLN - Pixels per line
	constant par_ppln  : natural  := 640 * 2 + par_hdw;
	-- VDW - Veritcal sync width
	constant par_vdw   : natural  := 5;
	-- LPFR - Lines per frame
	constant par_lpfr   : natural := 480 + par_vdw;
	-- SPH - Start pixel horizontal
	-- NPH - Size horizontal of valid area
	-- VD - Vertical sync
	-- OBS - Optical black start
	-- SLVx - Start line vertical field0 or field1
	-- NLV - Size vertical of valid area

	constant par_slv0  : natural := par_hdw - 1;


	signal h_pos_s : natural range 0 to (h_size_max - 1);
	signal v_pos_s : natural range 0 to (v_size_max - 1);
	signal h_pos_r : natural range 0 to (h_size_max - 1);
	signal v_pos_r : natural range 0 to (v_size_max - 1);

	signal clk_div_cnt_r : natural range 0 to (clk_div - 1);

	signal vsync_s : std_logic;
	signal hsync_s : std_logic;

	signal vsync_r : std_logic;
	signal hsync_r : std_logic;
	signal pclk_r :  std_logic;

	signal camemu_data_r : std_logic_vector(pixel_bits - 1 downto 0);

begin

	camemu_vsync_o <= vsync_r;
	camemu_hsync_o <= hsync_r;
	camemu_pclk_o  <= pclk_r;
	camemu_wen_o   <= '0';
	camemu_field_o <= '0';
	camemu_data_o  <= camemu_data_r;

comb:
	process (vsync_r, hsync_r, h_pos_r, v_pos_r)
	begin
		hsync_s <= hsync_r;
		h_pos_s <= h_pos_r;
		if h_pos_r = par_hdw then
			hsync_s <= not hsync_act_lev;
		end if;

		if h_pos_r = par_ppln then
			hsync_s <= hsync_act_lev;
			h_pos_s <= 0;
		else
			h_pos_s <= h_pos_r + 1;
		end if;

		vsync_s <= vsync_r;
		v_pos_s <= v_pos_r;
		if h_pos_r = par_slv0 then
			if v_pos_r = par_vdw then
				vsync_s <= not hsync_act_lev;
			end if;

			if v_pos_r = par_lpfr then
				vsync_s <= hsync_act_lev;
				v_pos_s <= 0;
			else
				v_pos_s <= v_pos_r + 1;
			end if;
		end if;
	end process;

seq: process
		variable val16: std_logic_vector(15 downto 0);
	begin
		wait until clk_i'event and clk_i = '1';
		if reset_i = '1' then
			clk_div_cnt_r <= 0;
			pclk_r <= '0';
			vsync_r <= '0';
			hsync_r <= '0';
			h_pos_r <= 0;
			v_pos_r <= 0;

		elsif clk_div_cnt_r /= 0 then 
			clk_div_cnt_r <= clk_div_cnt_r - 1;

		else
			clk_div_cnt_r <= clk_div - 1;
			if pclk_r = pclk_act_edge then
				pclk_r <= not pclk_act_edge;
			else
				pclk_r <= pclk_act_edge;
				vsync_r <= vsync_s;
				hsync_r <= hsync_s;
				h_pos_r <= h_pos_s;
				v_pos_r <= v_pos_s;

				val16 := std_logic_vector(to_unsigned(v_pos_r, 16));
				if val16(0) = '0' then
					val16 := std_logic_vector(to_unsigned(h_pos_r, 16));
					camemu_data_r <= val16(pixel_bits - 1 downto 0);
				else
					val16 := std_logic_vector(to_unsigned(v_pos_r, 16));
					camemu_data_r <= val16(pixel_bits - 1 downto 0);
				end if;
			end if;
		end if;
	end process;

end Behavioral;

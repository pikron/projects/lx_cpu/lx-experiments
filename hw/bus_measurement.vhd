library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lx_common_pkg.all;

-- Memory bus measurement
-- Holds the signal for one clock to simulate longest route

entity bus_measurement is
	port
	(
		-- Clock
		clk_i     : in std_logic;
		-- Reset
		reset_i   : in std_logic;
		-- Chip enable
		ce_i      : in std_logic;
		-- Address
		address_i : in std_logic_vector(1 downto 0);
		-- Data bus
		data_i    : in std_logic_vector(31 downto 0);
		data_o    : out std_logic_vector(31 downto 0);
		-- Bus signals
		bls_i     : in std_logic_vector(3 downto 0)
	);
end bus_measurement;

architecture Behavioral of bus_measurement is

	-- Wiring
	signal meas1_out_s    : std_logic_vector(31 downto 0);
	signal meas1_ce_s     : std_logic;
	--
	signal meas2_out_s    : std_logic_vector(31 downto 0);
	signal meas2_ce_s     : std_logic;

begin

	-- First measurement register (0xAAAAAAAA)
measurement1: measurement_register
	generic map
	(
		id_g   => "10101010101010101010101010101010"
	)
	port map
	(
		clk_i    => clk_i,
		ce_i     => meas1_ce_s,
		switch_i => address_i(0),
		reset_i  => reset_i,
		bls_i    => bls_i,
		data_i   => data_i,
		data_o   => meas1_out_s
	);

	-- Second measurement register (=0x55555555)
measurement2: measurement_register
	generic map
	(
		id_g   => "01010101010101010101010101010101"
	)
	port map
	(
		clk_i    => clk_i,
		ce_i     => meas2_ce_s,
		switch_i => address_i(0),
		reset_i  => reset_i,
		bls_i    => bls_i,
		data_i   => data_i,
		data_o   => meas2_out_s
	);

-- Bus process
update:
	process (ce_i, address_i, meas1_out_s, meas2_out_s)
	begin

		-- Defaults
		meas1_ce_s <= '0';
		meas2_ce_s <= '0';
		data_o     <= (others => 'X');

		-- Chip Enable
		if ce_i = '1' then
			if address_i(1) = '0' then
				meas1_ce_s  <= '1';
				data_o      <= meas1_out_s;
			else
				meas2_ce_s  <= '1';
				data_o      <= meas2_out_s;
			end if;
		end if;

	end process;

end Behavioral;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

-- Entities within lx_bram

package lx_bram_pkg is

	--------------------------------------------------------------------------------
	-- BRAM
	--------------------------------------------------------------------------------
	type BRAM_type is (READ_FIRST, WRITE_FIRST, NO_CHANGE);

	component xilinx_dualport_bram
	generic
	(
		byte_width    : positive := 8;
		address_width : positive := 8;
		we_width      : positive := 4;
		port_a_type   : BRAM_type := READ_FIRST;
		port_b_type   : BRAM_type := READ_FIRST
	);
	port
	(
		clka  : in std_logic;
		rsta  : in std_logic;
		ena   : in std_logic;
		wea   : in std_logic_vector((we_width-1) downto 0);
		addra : in std_logic_vector((address_width-1) downto 0);
		dina  : in std_logic_vector(((byte_width*we_width)-1) downto 0);
		douta : out std_logic_vector(((byte_width*we_width)-1) downto 0);
		clkb  : in std_logic;
		rstb  : in std_logic;
		enb   : in std_logic;
		web   : in std_logic_vector((we_width-1) downto 0);
		addrb : in std_logic_vector((address_width-1) downto 0);
		dinb  : in std_logic_vector(((byte_width*we_width)-1) downto 0);
		doutb : out std_logic_vector(((byte_width*we_width)-1) downto 0)
	);
	end component;

end lx_bram_pkg;

package body lx_bram_pkg is

end lx_bram_pkg;

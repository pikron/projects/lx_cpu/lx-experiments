library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

-- Entities within lx_exp_base

package lx_exp_base_pkg is

	-- Register on the bus
	component bus_register is
	generic
	(
		-- Reset value
		reset_value_g : std_logic_vector(31 downto 0) := (others => '0');
		-- Width
		b0_g          : natural := 8;
		b1_g          : natural := 8;
		b2_g          : natural := 8;
		b3_g          : natural := 8
	);
	port
	(
		-- Clock
		clk_i         : in std_logic;
		-- Reset
		reset_i       : in std_logic;
		-- Chip enable
		ce_i          : in std_logic;
		-- Data bus
		data_i        : in std_logic_vector((b0_g+b1_g+b2_g+b3_g-1) downto 0);
		data_o        : out std_logic_vector((b0_g+b1_g+b2_g+b3_g-1) downto 0);
		-- Bus signals
		bls_i         : in std_logic_vector(3 downto 0)
	);
	end component;


	--------------------------------------------------------------------------------
	-- BRAM
	--------------------------------------------------------------------------------
	type BRAM_type is (READ_FIRST, WRITE_FIRST, NO_CHANGE);

	component xilinx_dualport_bram
	generic
	(
		byte_width    : positive := 8;
		address_width : positive := 8;
		we_width      : positive := 4;
		port_a_type   : BRAM_type := READ_FIRST;
		port_b_type   : BRAM_type := READ_FIRST
	);
	port
	(
		clka  : in std_logic;
		rsta  : in std_logic;
		ena   : in std_logic;
		wea   : in std_logic_vector((we_width-1) downto 0);
		addra : in std_logic_vector((address_width-1) downto 0);
		dina  : in std_logic_vector(((byte_width*we_width)-1) downto 0);
		douta : out std_logic_vector(((byte_width*we_width)-1) downto 0);
		clkb  : in std_logic;
		rstb  : in std_logic;
		enb   : in std_logic;
		web   : in std_logic_vector((we_width-1) downto 0);
		addrb : in std_logic_vector((address_width-1) downto 0);
		dinb  : in std_logic_vector(((byte_width*we_width)-1) downto 0);
		doutb : out std_logic_vector(((byte_width*we_width)-1) downto 0)
	);
	end component;

end lx_exp_base_pkg;

package body lx_exp_base_pkg is

end lx_exp_base_pkg;

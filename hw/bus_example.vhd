library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lx_common_pkg.all;

-- Connects example memory

entity bus_example is
	port
	(
		-- Clock
		clk_i        : in std_logic;
		-- Chip enable
		ce_i         : in std_logic;
		-- Global Reset
		reset_i      : in std_logic;
		-- Master CPU peripheral bus
		bls_i        : in std_logic_vector(3 downto 0);
		address_i    : in std_logic_vector(11 downto 0);
		data_i       : in std_logic_vector(31 downto 0);
		data_o       : out std_logic_vector(31 downto 0)

		-- Non bus signals
		--
		-- Add there external component signals
  );
end bus_example;

architecture Behavioral of bus_example is

	signal example_mem_ce_s   : std_logic;
	signal example_mem_ce_r   : std_logic;
	signal example_mem_bls_s  : std_logic_vector(3 downto 0);
	signal example_mem_dout_s : std_logic_vector(31 downto 0);
begin

example_mem_instance: lx_example_mem
	port map
	(
		-- Memory wiring for internal state automata use
		clk_i  => clk_i,
		ce_i   => '0',
		adr_i  => (others => '0'),
		bls_i  => (others => '0'),
		dat_i  => (others => '0'),
		dat_o  => open,
		-- Memory wiring for Master CPU
		clk_m  => clk_i,
		en_m   => example_mem_ce_s,
		we_m   => example_mem_bls_s,
		addr_m => address_i(9 downto 0),
		din_m  => data_i,
		dout_m => example_mem_dout_s
	);

decoder_logic: process(ce_i, address_i, bls_i)
        begin
		example_mem_ce_s <= '0';
		example_mem_bls_s <= (others => '0');

		if ce_i = '1' and address_i(11 downto 10) = "00" then
			example_mem_ce_s <= '1';
			example_mem_bls_s <= bls_i;
		end if;
	end process;

output_multiplexer: process(example_mem_ce_r, example_mem_dout_s)
        begin
		data_o <= (others => '0');

		if example_mem_ce_r = '1' then
			data_o <= example_mem_dout_s;
		end if;
	end process;

sync_update:
	process
	begin
		wait until clk_i = '1' and clk_i'event;

		example_mem_ce_r <= example_mem_ce_s;
	end process;


end Behavioral;

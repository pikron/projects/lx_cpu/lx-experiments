library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

-- Entities within lx_common

package lx_common_pkg is

	-- D sampler (filtered, 2 cycles)
	component dff2
	port
	(
		clk_i   : in std_logic;
		d_i     : in std_logic;
		q_o     : out std_logic
	);
	end component;

	-- D sampler (filtered, 3 cycles)
	component dff3
	port
	(
		clk_i   : in std_logic;
		d_i     : in std_logic;
		q_o     : out std_logic
	);
	end component;

	-- Counter - divider
	component cnt_div
	generic (
		cnt_width_g : natural := 8
	);
	port
	(
		clk_i     : in std_logic;
		en_i      : in std_logic;
		reset_i   : in std_logic;
		ratio_i   : in std_logic_vector(cnt_width_g-1 downto 0);
		q_out_o   : out std_logic
	);
	end component;

	-- Clock Cross Domain Synchronization Elastic Buffer/FIFO
	component lx_crosdom_ser_fifo
	generic
	(
		fifo_len_g   : positive := 8;
		sync_adj_g   : integer := 0
	);
	port
	(
		-- Asynchronous clock domain interface
		acd_clock_i  : in std_logic;
		acd_miso_i   : in std_logic;
		acd_sync_i   : in std_logic;
		-- Clock
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Output synchronous with clk_i
		miso_o       : out std_logic;
		sync_o       : out std_logic;
		data_ready_o : out std_logic
	);
	end component;

	--------------------------------------------------------------------------------
	-- MEMORY BUS
	--------------------------------------------------------------------------------

	-- Measurement register
	component measurement_register
	generic
	(
		id_g   : std_logic_vector(31 downto 0) := (others => '0')
	);
	port
	(
		-- Clock
		clk_i    : in std_logic;
		-- Reset
		reset_i  : in std_logic;
		-- Chip enable
		ce_i     : in std_logic;
		-- Switch
		switch_i : in std_logic;
		-- Data bus
		data_i   : in std_logic_vector(31 downto 0);
		data_o   : out std_logic_vector(31 downto 0);
		-- Bus signals
		bls_i    : in std_logic_vector(3 downto 0)
	);
	end component;

	-- Example component interconnect
	component bus_example
	port
	(
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Master CPU peripheral bus
		address_i    : in std_logic_vector(11 downto 0);
		ce_i         : in std_logic;
		data_i       : in std_logic_vector(31 downto 0);
		data_o       : out std_logic_vector(31 downto 0);
		--
		bls_i        : in std_logic_vector(3 downto 0)

		-- Non bus signals
		--
		-- Add there externaly visible signals
	);
	end component;

	-- Dualported memory for example componenet
	component lx_example_mem
	port
	(
		-- Memory wiring for internal state automata use
		clk_i  : in std_logic;
		ce_i   : in std_logic;
		adr_i  : in std_logic_vector(9 downto 0);
		bls_i  : in std_logic_vector(3 downto 0);
		dat_i  : in std_logic_vector(31 downto 0);
		dat_o  : out std_logic_vector(31 downto 0);
		-- Memory wiring for Master CPU
		clk_m  : in std_logic;
		en_m   : in std_logic;
		we_m   : in std_logic_vector(3 downto 0);
		addr_m : in std_logic_vector(9 downto 0);
		din_m  : in std_logic_vector(31 downto 0);
		dout_m : out std_logic_vector(31 downto 0)
	);
	end component;

	-- Measurement interconnect
	component bus_measurement
	port
	(
		-- Clock
		clk_i     : in std_logic;
		-- Reset
		reset_i   : in std_logic;
		-- Chip enable
		ce_i      : in std_logic;
		-- Address
		address_i : in std_logic_vector(1 downto 0);
		-- Data bus
		data_i    : in std_logic_vector(31 downto 0);
		data_o    : out std_logic_vector(31 downto 0);
		-- Bus signals
		bls_i     : in std_logic_vector(3 downto 0)
	);
	end component;

end lx_common_pkg;

package body lx_common_pkg is

end lx_common_pkg;

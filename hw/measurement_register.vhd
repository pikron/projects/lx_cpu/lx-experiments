library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lx_common_pkg.all;

-- Transaction measurement register

entity measurement_register is
	generic
	(
		id_g   : std_logic_vector(31 downto 0) := (others => '0')
	);
	port
	(
		-- Clock
		clk_i    : in std_logic;
		-- Reset
		reset_i  : in std_logic;
		-- Chip enable
		ce_i     : in std_logic;
		-- Switch
		switch_i : in std_logic;
		-- Data bus
		data_i   : in std_logic_vector(31 downto 0);
		data_o   : out std_logic_vector(31 downto 0);
		-- Bus signals
		bls_i    : in std_logic_vector(3 downto 0)
	);
end measurement_register;

architecture Behavioral of measurement_register is
	signal value_s : std_logic_vector(31 downto 0);
begin

	data_o <= value_s when switch_i = '1' else id_g;

-- Write waits for clock
memory_bus_write:
	process
	begin

		wait until clk_i'event and clk_i = '1';

		if reset_i = '1' then
			value_s <= (others => '0');
		else

			if ce_i = '1' and bls_i /= "0000" then

				if bls_i(0) = '1' then
					value_s(7 downto 0)   <= data_i(7 downto 0);
				end if;
				if bls_i(1) = '1' then
					value_s(15 downto 8)  <= data_i(15 downto 8);
				end if;
				if bls_i(2) = '1' then
					value_s(23 downto 16) <= data_i(23 downto 16);
				end if;
				if bls_i(3) = '1' then
					value_s(31 downto 24) <= data_i(31 downto 24);
				end if;

			end if;
		end if;

	end process;

end Behavioral;

